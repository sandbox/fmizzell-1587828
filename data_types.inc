<?php

class Text extends SimplePropertyHandler{
  
  public function __construct($data) {
    parent::__construct($data);
  }
  
  public static function propertyHandlerInit($info, $data){
    return new Text($data);
  }
  
  protected function validate($value){
    return is_string($value);
  }
}

class Integer extends SimplePropertyHandler{
  
  public function __construct($data) {
    parent::__construct($data);
  }

  public static function propertyHandlerInit($info, $data){
    return new Integer($data);
  }
  
  protected function validate($value) {
    return is_integer($value);
  }
}

//A Date (timestamp) is an integer
class Date extends Integer{
  
  public function setDate($year, $month, $day){
    $date = new DateTime("{$year}-{$month}-{$day}");
    $this->data = $date->getTimestamp();
  }
}
